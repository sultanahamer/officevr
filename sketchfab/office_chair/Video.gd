extends Spatial

# Called when the node enters the scene tree for the first time.
func _ready():
  if $Sprite3D.texture.camera_is_active :
    return;
  $Sprite3D.texture.camera_is_active = true

 
func _on_Timer_timeout():
  # hack to make sure the textures are rendering. They don't if video isn't available right away
  $Sprite3D.flip_h = true
  $Sprite3D.flip_h = false
